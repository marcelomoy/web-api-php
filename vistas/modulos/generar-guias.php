 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
     <h1>
       Generar Guías
     </h1>
     <ol class="breadcrumb">
       <li><a href="#"><i class="fa fa-dashboard"></i> inicio</a></li>
       <li class="active">Generar Guías</li>
     </ol>
   </section>

   <!-- Main content -->
   <section class="content">

     <!-- Default box -->
     <div class="box">
       <div class="box-header with-border">
         <h3 class="box-title">Title</h3>


       </div>
       <div class="box-body">

         <div class="col-md-6">
           <h2>Origen</h2>

           <div style="margin-top:4%" class="col-md-5">
             <div>
               <label>*Nombre</label>
             </div>
             <div>
               <input name="lastn" type="email" autocomplete="off" class="form-control">
               <div class="form-control-focus"> </div>
             </div>

             <div>
               <label>*Email</label>
             </div>
             <div>
               <input name="lastn" type="email" autocomplete="off" class="form-control">
               <div class="form-control-focus"> </div>
             </div>
           </div>

           <div style="margin-top:4%" class="col-md-5">
             <div>
               <label>*Empresa</label>
             </div>
             <div>
               <input name="lastn" type="email" autocomplete="off" class="form-control">
               <div class="form-control-focus"> </div>
             </div>

             <div>
               <label>*Teléfono</label>
             </div>
             <div>
               <input name="lastn" type="email" autocomplete="off" class="form-control">
               <div class="form-control-focus"> </div>
             </div>
           </div>

           <div class="clearfix"></div>

           <div class="col-md-10" style="margin-top:4%">

             <div>
               <div>
                 <label>*País</label>
               </div>
               <div>
                 <select name="lastn" type="email" autocomplete="off" class="form-control">
                   <option>México</option>
                 </select>

                 <div class="form-control-focus"> </div>
               </div>
             </div>
             <div class="clearfix"></div>

             <div style="margin-top:4%">
               <div>
                 <label>*Calle</label>
               </div>
               <div>
                 <input name="lastn" type="email" autocomplete="off" class="form-control">
                 <div class="form-control-focus"> </div>
               </div>
             </div>
             <div class="clearfix"></div>

             <div style="margin-top:4%">
               <div>
                 <label>*Número</label>
               </div>
               <div>
                 <input name="lastn" type="email" autocomplete="off" class="form-control">
                 <div class="form-control-focus"> </div>
               </div>
             </div>

           </div>

           <div style="margin-top:4%" class="col-md-5">
             <div>
               <label>*Código Postal</label>
             </div>
             <div>
               <input name="lastn" type="email" autocomplete="off" class="form-control">
               <div class="form-control-focus"> </div>
             </div>

             <div>
               <label>*Ciudad</label>
             </div>
             <div>
               <input name="lastn" type="email" autocomplete="off" class="form-control">
               <div class="form-control-focus"> </div>
             </div>
           </div>


           <div style="margin-top:4%" class="col-md-5">
             <div>
               <label>*Colonia</label>
             </div>
             <div>
               <input name="lastn" type="email" autocomplete="off" class="form-control">
               <div class="form-control-focus"> </div>
             </div>

             <div>
               <label>*Estado</label>
             </div>
             <div>
               <select name="lastn" type="email" autocomplete="off" class="form-control">
                 <option>Nuevo León</option>
                 <option>CDMX</option>
                 <option>Aguascalientes</option>
                 <option>Guadalajara</option>
               </select>
               <div class="form-control-focus"> </div>
             </div>
           </div>

           <div class="col-md-10" style="margin-top:3%">
             <div>
               <label>Referencia</label>
             </div>
             <div>
               <input name="lastn" type="email" autocomplete="off" class="form-control">
               <div class="form-control-focus"> </div>
             </div>
           </div>


         </div>




         <div class="col-md-6">
           <h2>Destino</h2>

           <div style="margin-top:4%" class="col-md-5">
             <div>
               <label>*Nombre</label>
             </div>
             <div>
               <input name="lastn" type="email" autocomplete="off" class="form-control">
               <div class="form-control-focus"> </div>
             </div>

             <div>
               <label>*Email</label>
             </div>
             <div>
               <input name="lastn" type="email" autocomplete="off" class="form-control">
               <div class="form-control-focus"> </div>
             </div>
           </div>

           <div style="margin-top:4%" class="col-md-5">
             <div>
               <label>*Empresa</label>
             </div>
             <div>
               <input name="lastn" type="email" autocomplete="off" class="form-control">
               <div class="form-control-focus"> </div>
             </div>

             <div>
               <label>*Teléfono</label>
             </div>
             <div>
               <input name="lastn" type="email" autocomplete="off" class="form-control">
               <div class="form-control-focus"> </div>
             </div>
           </div>

           <div class="clearfix"></div>

           <div class="col-md-10" style="margin-top:4%">

             <div>
               <div>
                 <label>*País</label>
               </div>
               <div>
                 <select name="lastn" type="email" autocomplete="off" class="form-control">
                   <option>México</option>
                 </select>
                 <div class="form-control-focus"> </div>
               </div>
             </div>
             <div class="clearfix"></div>

             <div style="margin-top:4%">
               <div>
                 <label>*Calle</label>
               </div>
               <div>
                 <input name="lastn" type="email" autocomplete="off" class="form-control">
                 <div class="form-control-focus"> </div>
               </div>
             </div>
             <div class="clearfix"></div>

             <div style="margin-top:4%">
               <div>
                 <label>*Número</label>
               </div>
               <div>
                 <input name="lastn" type="email" autocomplete="off" class="form-control">
                 <div class="form-control-focus"> </div>
               </div>
             </div>

           </div>

           <div style="margin-top:4%" class="col-md-5">
             <div>
               <label>*Código Postal</label>
             </div>
             <div>
               <input name="lastn" type="email" autocomplete="off" class="form-control">
               <div class="form-control-focus"> </div>
             </div>

             <div>
               <label>*Ciudad</label>
             </div>
             <div>
               <input name="lastn" type="email" autocomplete="off" class="form-control">
               <div class="form-control-focus"> </div>
             </div>
           </div>


           <div style="margin-top:4%" class="col-md-5">
             <div>
               <label>*Colonia</label>
             </div>
             <div>
               <input name="lastn" type="email" autocomplete="off" class="form-control">
               <div class="form-control-focus"> </div>
             </div>

             <div>
               <label>*Estado</label>
             </div>
             <div>
               <select name="lastn" type="email" autocomplete="off" class="form-control">
                 <option>Nuevo León</option>
                 <option>CDMX</option>
                 <option>Aguascalientes</option>
                 <option>Guadalajara</option>
               </select>
               <div class="form-control-focus"> </div>
             </div>
           </div>

           <div class="col-md-10" style="margin-top:3%">
             <div>
               <label>Referencia</label>
             </div>
             <div>
               <input name="lastn" type="email" autocomplete="off" class="form-control">
               <div class="form-control-focus"> </div>
             </div>
           </div>


         </div>
         <div class="clearfix"></div>

         <div class="col-md-6" style="margin-top:5%">
           <h2>Información Del Envío</h2>

           <div style="margin-top:4%" class="col-md-5">
             <div>
               <label>*Contenido del paquete</label>
             </div>
             <div>
               <input name="lastn" type="email" autocomplete="off" class="form-control">
               <div class="form-control-focus"> </div>
             </div>
           </div>
           <div style="margin-top:4%" class="col-md-5">
             <div>
               <label>*Seguro (opcional)</label>
             </div>
             <div>
               <input name="lastn" type="email" autocomplete="off" class="form-control">
               <div class="form-control-focus"> </div>
             </div>
           </div>
           <div class="clearfix"></div>


           <div style="margin-top:4%">
           <h2 >Medidas del paquete</h2>
           </div>

           <div class="col-md-3">
             <div>
               <label>*Seguro (opcional)</label>
             </div>
             <div>
               <input name="lastn" type="email" autocomplete="off" class="form-control">
               <div class="form-control-focus"> </div>
             </div>
           </div>
           <div class="col-md-3">
             <div>
               <label>*Seguro (opcional)</label>
             </div>
             <div>
               <input name="lastn" type="email" autocomplete="off" class="form-control">
               <div class="form-control-focus"> </div>
             </div>
           </div>
           <div class="col-md-3">
             <div>
               <label>*Seguro (opcional)</label>
             </div>
             <div>
               <input name="lastn" type="email" autocomplete="off" class="form-control">
               <div class="form-control-focus"> </div>
             </div>
           </div>
           <div class="col-md-3">
             <div>
               <label>*Seguro (opcional)</label>
             </div>
             <div>
               <input name="lastn" type="email" autocomplete="off" class="form-control">
               <div class="form-control-focus"> </div>
             </div>
           </div>


         </div>


         <!-- /.box-body -->

         <!-- /.box-footer-->
       </div>
       <!-- /.box -->

   </section>
   <!-- /.content -->
 </div>