<aside class="main-sidebar">

	<section class="sidebar">

		<ul class="sidebar-menu">

			<li class="active">

				<a href="inicio">

					<i class="fa fa-asterisk"></i>
					<span>Inicio</span>

				</a>

			</li>

			<li>

				<a href="generar-guias">

					<i class="fa fa-map-marker"></i>
					<span>Generar Guías</span>

				</a>

			</li>

			<li>

				<a href="obtener-guias-generadas">

					<i class="fa fa-map"></i>
					<span>Obtener Guías Generadas</span>

				</a>

			</li>

			<li>

				<a href="realizar-cancelaciones">

					<i class="fa fa-calendar-times-o"></i>
					<span>Realizar Cancelaciónes</span>

				</a>

			</li>


			<li>

				<a href="informacion-de-cancelaciones">

					<i class="fa fa-binoculars"></i>
					<span>Información De Cancelaciónes</span>

				</a>

			</li>



		</ul>

	</section>

</aside>